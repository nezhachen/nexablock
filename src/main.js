// Inside your main.js or main.ts
import './assets/main.css';
import { createApp } from 'vue';
import App from './App.vue';
import { createPinia } from 'pinia';
import piniaPluginPersist from 'pinia-plugin-persist';

const pinia = createPinia();
pinia.use(piniaPluginPersist);
pinia.use(({ store }) => {
    if (store.$id === 'theme') {
        store.initializeTheme();
    }
});

pinia.use(({ store }) => {
    if (store.$id === 'stats') {
        store.checkAndFetchMarketData();
        store.checkAndFetchExplorerData();
        store.fetchHalvingDate();
    }
});

const app = createApp(App);
app.use(pinia);
app.mount('#app');