import { defineStore } from 'pinia';
import { DateTime } from 'luxon';
import axios from 'axios';


const calculateBlocksTillHalving = (n) => {
    const halvings = [210000 * 5, 210000 * 5 * 2, 210000 * 5 * 3, 210000 * 5 * 4, 210000 * 5 * 5, 210000 * 5 * 6, 210000 * 5 * 7];
    // Find the next larger number
    const nextLarger = halvings.find(num => num > n);
    
    // If there's no next larger number, return 0
    if (!nextLarger) {
        return 0;
    }
    
    // Calculate the number of blocks till the next halving
    const blocksTillHalving = nextLarger - n;
    
    return blocksTillHalving;
}

const calculateDateTimeOfHalving = (n) => {
    // Calculate the number of blocks till the next halving
    const blocksTillHalving = calculateBlocksTillHalving(n);
    
    // If there are no blocks till halving, return null
    if (blocksTillHalving === 0) {
        return null;
    }
    
    // Calculate the number of days till the next halving
    const daysTillHalving = blocksTillHalving / 720;
    
    // Calculate the date and time of the next halving
    const currentDate = new Date();
    const nextHalvingDate = new Date(currentDate.getTime() + daysTillHalving * 24 * 60 * 60 * 1000); // Add milliseconds equivalent to daysTillHalving
    return DateTime.fromObject({ year: nextHalvingDate.getFullYear(), month: nextHalvingDate.getMonth() + 1, day: nextHalvingDate.getDate(), hour: nextHalvingDate.getHours(), minute: nextHalvingDate.getMinutes(), second: nextHalvingDate.getSeconds() }).toISO();
}

export const useThemeStore = defineStore('theme', {
    state: () => ({
        theme: 'light', // Set a default theme
        dark: false,
    }),
    actions: {
        initializeTheme() {
            const storedTheme = localStorage.getItem('theme');
            if (storedTheme) {
                this.setTheme(storedTheme);
            } else {
                this.setTheme('system'); // Default to system theme on first load
            }
        },
        setTheme(theme) {
            if (theme === 'system') {
                const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
                this.theme = prefersDarkMode ? 'dark' : 'light';
            } else {
                this.theme = theme;
            }
            this.dark = this.theme === 'dark';
            document.documentElement.setAttribute('data-theme', this.theme);
            localStorage.setItem('theme', this.theme);
        },
    },
});

export const useStatsStore = defineStore('stats', {
    state: () => ({
        marketData: null,
        explorerData: null,
        lastFetched: null, // Will store as an ISO string
        targetDate: null
    }),
    actions: {
        async fetchMarketData() {
            const apiKey = import.meta.env.VITE_COINGECKO_API_KEY;
            const config = {
                headers: {
                    'Authorization': `Bearer ${apiKey}`,
                },
            };
            try {
                const response = await axios.get('https://api.coingecko.com/api/v3/coins/nexacoin', config);
                this.marketData = response.data.market_data;
                // Use Luxon to get the current time in ISO format
                this.lastFetched = DateTime.local().toISO();
            } catch (error) {
                console.error("There was an error fetching the CoinGecko data:", error);
            }
        },
        checkAndFetchMarketData() {
            if (!this.lastFetched) {
                this.fetchMarketData();
                return;
            }

            const now = DateTime.local();
            const lastFetchedTime = DateTime.fromISO(this.lastFetched);
            const diff = now.diff(lastFetchedTime, ['minutes']).minutes;

            if (diff >= 5) {
                // If more than 5 minutes have passed, fetch immediately.
                this.fetchMarketData();
            } else {
                // Calculate delay for less than 5 minutes.
                const delay = (5 - diff) * 60 * 1000; // Time until next refresh
                setTimeout(() => {
                    this.fetchMarketData();
                }, delay);
            }

            // Set to refresh every 5 minutes after either immediate fetch or delayed fetch.
            setInterval(() => {
                this.fetchMarketData();
            }, 5 * 60 * 1000);
        },
        async checkAndFetchExplorerData() {
            try {
                const response = await axios.get('https://explorer.nexa.org/api/blocks');
                this.explorerData = response.data;
            } catch (error) {
                console.error("There was an error fetching the Explorer data:", error);
            }
        },
        async fetchHalvingDate(){
            try {
                if(this.explorerData == null) {
                    await this.checkAndFetchExplorerData()
                }
                let totalBlocks = this.explorerData.blockChainInfo?.blocks;
    
                this.targetDate = calculateDateTimeOfHalving(totalBlocks)
                return this.targetDate;
            }catch (err) {
                console.error("There was an error fetching the Explorer data:", err);
                return null;
            }

        }
    },
    // Enable persistence
    persist: {
        enabled: true,
        strategies: [{ storage: localStorage }]
    }
});