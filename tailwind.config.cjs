/** @type {import('tailwindcss').Config} */
import daisyui from "daisyui";
import { light as defaultLightTheme, dark as defaultDarkTheme } from "daisyui/src/theming/themes";

export default {
  content: ["./src/**/*.{html,js,vue}", 'index.html'],
  theme: {
    container: {
      center: true,
      padding: {
        "DEFAULT": "1rem",
        "sm": "2rem",
        "lg": "4rem",
        "xl": "6rem",
        "2xl": "8rem",
      },
    },
    fontFamily: {
      body: ["'DM Sans'", "sans-serif"],
    },
  },
  daisyui: {
    themes: [
      {
        light: {
          ...defaultLightTheme,
          "primary": "#3C2F48",
          "primary-content": "#ffffff",
          "primary-branding": "#3C2F48",
          "secondary": "#494949",
          "neutral": "#03131a",
          "info": "#00e1ff",
          "success": "#90ca27",
          "warning": "#ff8800",
          "error": "#ff7f7f",
          "--rounded-box": "0.25rem",
          "--rounded-btn": "0.25rem",
        },
        dark: {
          ...defaultDarkTheme,
          "primary": "#E1E1E1",
          "primary-content": "#ffffff",
          "primary-branding": "#E1E1E1",
          "secondary": "#494949",
          "neutral": "#03131a",
          "info": "#00e1ff",
          "success": "#90ca27",
          "warning": "#ff8800",
          "error": "#ff7f7f",
          "base-100": "#14181c",
          "base-200": "#1e2328",
          "base-300": "#28323c",
          "base-content": "#dcebfa",
          "--rounded-box": "0.25rem",
          "--rounded-btn": "0.25rem",
        },
      },
    ],
  },
  plugins: [daisyui],
}
