
# Nexablock

## Overview

Welcome to Nexablock, a static website designed to show a visual countdown to Nexa's upcoming halving events.
This project utilizes Vue.js, Vite, Tailwind and DaisyUI.

## Getting Started

### Prerequisites

Before you begin, ensure you have the following installed:
- **Node.js**: Vite requires Node.js to run. The version requirements for this project is **Node.js 18 / 20+**
- **npm or Yarn**: These are Node.js package managers used to install the necessary packages and run commands for development and build processes. npm is installed automatically with Node.js. Yarn is an alternative that some developers prefer and can be installed separately.

### Installation

1. **Clone the repository**

   ```sh
   git clone https://gitlab.com/nexa/nexablock.git
   ```

2. **Navigate to the project directory**

   ```sh
   cd path/to/Nexablock
   ```

3. **Install dependencies**

   ```sh
   npm install
   ```

4. **Run the project locally**

   ```sh
   npm run dev
   ```

   The site should now be running on [http://localhost](http://localhost). The port number will be displayed in your terminal.

## Deployment

To deploy this project live, you can use:
- [GitLab](https://gitlab.com) - Directly from the repository.

## Contributing

Please follow these steps to contribute:

1. Fork the repository.
2. Create a new branch: `git checkout -b feature/YourFeature`.
3. Make your changes and commit them: `git commit -am 'Add some feature'`.
4. Push to the branch: `git push origin feature/YourFeature`.
5. Create a new Pull Request.
